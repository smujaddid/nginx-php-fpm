
## Overview
This image unified both NGINX and PHP-FPM in one container with the ability to pull source code from git to serve a website.
Alpine based PHP-FPM is used as base image, NGINX installed by using the same script as NGINX Dockerfile on its GitHub repository.

### Versions

+ `latest`, `php-7.4-nginx-1.18`
+ `preview`, `php-8.0-nginx-1.19`

### Installed PHP Extensions

Other than default extensions that is installed in the php base image, the following extensions are enabled:

+ bcmath
+ exif
+ gd
+ gmp
+ intl
+ mysqli
+ opcache
+ pdo_mysql
+ pdo_pgsql
+ pgsql
+ redis
+ soap
+ xsl
+ zip

### Installed Alpine Packages

+ bash
+ ca-certificates
+ curl
+ git
+ jq
+ libcurl
+ nodejs
+ npm
+ openssh-client
+ su-exec
+ supervisor
+ tzdata
+ wget

### Installed Tools

+ Wordpres CLI 2.x
+ Composer 2.x


## Usage

Pull from docker
```shell
docker pull smujaddid/nginx-php-fpm:latest
```

Run the container
```shell
docker run -d smujaddid/nginx-php-fpm:latest
```

## Available Configuration

| Key                       | Description |
|---------------------------|----------------------|
| GIT_REPO                  | URL to your repository without the http:// or https:// |
| GIT_BRANCH                | The default branch to be pulled |
| GIT_TAG                   | The tag that will be checked out |
| GIT_COMMIT                | The commit that will be checked out |
| GIT_EMAIL                 | Email for git |
| GIT_NAME                  | Name for git |
| GIT_USERNAME              | Git username |
| GIT_PERSONAL_TOKEN        | Personal access token for accessing the git repository |
| REMOVE_FILES              | Set to `1` to remove files before pulling source code |
| SKIP_COMPOSER             | Set to `1` to skip running composer automatically when `composer.lock` detected |
| SKIP_LARAVEL_ARTISAN      | Set to `1` to skip running laravel artisan automatically when `artisan` detected |
| SKIP_NPM                  | Set to `1` to skip running npm automatically when `package-lock.json` detected |
| NPM_RUN                   | Call `npm run prod` or `npm run dev` for node projects |
| APPLICATION_ENV           | Set the image as either `production` or `development` environment. This will affect `composer --dev` and `npm run dev` |
| RUN_SCRIPTS               | Set to `1` to allow scripts in `./script/` folder to be running at the end of setup |
| PHP_CATCHALL              | Set to `1` to allow serve Laravel by directing all query to index.php |
| SHOW_NGINX_HEADERS        | Set to `1` to show nginx version. Default `0`. |
| REAL_IP_HEADER            | Set to `1` to enable Real IP support |
| REAL_IP_FROM              | Set CIDR block for Real IP |
| PHP_MEM_LIMIT             | Set PHP memory_limit. Default 128. |
| PHP_POST_MAX_SIZE         | Set PHP post_max_size. Default 100. |
| PHP_UPLOAD_MAX_FILESIZE   | Set PHP upload_max_filesize. Default 100. |
| PHP_MAX_EXECUTION_TIME   | Set PHP max_execution_time. Default 600. |
| PUID                      | Set user id for PHP user. Source code will be owned by this user. |
| GUID                      | Set group id for PHP user. Source code will be assigned to this user group. |
| SKIP_CHOWN                | Do not change ownership of source code |
| WEBROOT                   | Change the default webroot (`/var/www/html`) in nginx host configuration |
| RUN_LARAVEL_WORKER        | Set to `1` to run Laravel queue worker automatically using supervisord |
| RUN_LARAVEL_WEBSOCKETS    | Set to `1` to run Laravel websockets automatically using supervisord. Requires 'composer install beyondcode/laravel-websockets' |
| RUN_LARAVEL_SCHEDULER     | Set to `1` to run Laravel scheduler automatically using supervisord |