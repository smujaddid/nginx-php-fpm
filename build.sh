#!/bin/bash

TAG="latest"

if [ ! -z "$1" ]; then
  TAG=$1
fi

docker build --compress -t smujaddid/nginx-php-fpm:$TAG -f Dockerfile-php-8.1 .
