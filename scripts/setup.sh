#!/bin/bash

# Force reassign APP_ROOT to /var/ww/html
APP_ROOT=/var/www/html

# Disable Strict Host checking for non interactive git clones
mkdir -p -m 0700 $PHP_HOME/.ssh
# Prevent config files from being filled to infinity by force of stop and restart the container 
echo "" > $PHP_HOME/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n" >> $PHP_HOME/.ssh/config

if [[ "$GIT_USE_SSH" == "1" ]] ; then
  echo -e "Host *\n\tUser ${GIT_USERNAME}\n\n" >> $PHP_HOME/.ssh/config
fi

if [[ ! -z "$SSH_KEY" ]]; then
  echo $SSH_KEY > $PHP_HOME/.ssh/id_rsa.base64
  base64 -d $PHP_HOME/.ssh/id_rsa.base64 > $PHP_HOME/.ssh/id_rsa
  chmod 600 $PHP_HOME/.ssh/id_rsa
fi

# Setup git variables
if [[ ! -z "$GIT_EMAIL" ]]; then
  git config --global user.email "$GIT_EMAIL"
fi
if [[ ! -z "$GIT_NAME" ]]; then
  git config --global user.name "$GIT_NAME"
fi
# Set git push.default to simple as default to suppress warning
git config --global push.default simple
# Set git pull.rebase to true to avoid non-linear history
git config --global pull.rebase true
# Set diff color
git config --global diff.colorMoved zebra

if [[ ! -z "$GIT_USERNAME" ]] && [[ ! -z "$GIT_PERSONAL_TOKEN" ]]; then
  # Setup Gitlab Url with access token
  git config --global --add url."https://${GIT_USERNAME}:${GIT_PERSONAL_TOKEN}@gitlab.com/".insteadOf "https://gitlab.com/"
  git config --global --add url."https://${GIT_USERNAME}:${GIT_PERSONAL_TOKEN}@gitlab.com/".insteadOf "git@gitlab.com:"
fi

# Dont pull code down if the .git folder exists
if [[ ! -d "$APP_ROOT/.git" ]]; then
  # Pull down code from git for our site!
  if [[ ! -z "$GIT_REPO" ]]; then
    # Remove the test index file if you are pulling in a git repo
    if [[ ! -z ${REMOVE_FILES} ]] && [[ ${REMOVE_FILES} == 0 ]]; then
      echo "skiping removal of files"
    else
      rm -Rf $APP_ROOT/*
    fi
    GIT_COMMAND='git clone --depth=1 -q '
    if [[ ! -z "$GIT_BRANCH" ]]; then
      GIT_COMMAND=${GIT_COMMAND}" -b ${GIT_BRANCH}"
    fi

    if [[ -z "$GIT_USERNAME" ]] && [[ -z "$GIT_PERSONAL_TOKEN" ]]; then
      GIT_COMMAND=${GIT_COMMAND}" ${GIT_REPO}"
    else
      if [[ "$GIT_USE_SSH" == "1" ]]; then
        GIT_COMMAND=${GIT_COMMAND}" ${GIT_REPO}"
      else
        GIT_COMMAND=${GIT_COMMAND}" https://${GIT_USERNAME}:${GIT_PERSONAL_TOKEN}@${GIT_REPO}"
      fi
    fi
    ${GIT_COMMAND} $APP_ROOT || exit 1
    if [[ ! -z "$GIT_TAG" ]]; then
      git checkout ${GIT_TAG} || exit 1
    fi
    if [[ ! -z "$GIT_COMMIT" ]]; then
      git checkout ${GIT_COMMIT} || exit 1
    fi
  fi
fi

# Setup git variables locally
if [[ ! -z "$GIT_EMAIL" ]]; then
  git config user.email "$GIT_EMAIL"
fi
if [[ ! -z "$GIT_NAME" ]]; then
  git config user.name "$GIT_NAME"
fi
# Set git push.default to simple as default to suppress warning
git config push.default simple

if [[ -z "$SKIP_COMPOSER" ]]; then
  # Try auto install for composer
  if [[ -f "$APP_ROOT/composer.lock" ]]; then
    if [[ ! -z "$GITLAB_ACCESS_TOKEN" ]]; then
      composer config --global gitlab-token.gitlab.com $GITLAB_ACCESS_TOKEN
    fi
    if [[ ! -z "$GITLAB_OAUTH_TOKEN" ]]; then
      composer config --global gitlab-oauth.gitlab.com $GITLAB_OAUTH_TOKEN
    fi
    if [[ ! -z "$GITHUB_OAUTH_TOKEN" ]]; then
      composer config --global github-oauth.github.com $GITHUB_OAUTH_TOKEN
    fi
    if [[ ! -z "$BITBUCKET_CONSUMER_KEY" ]] && [[ ! -z "$BITBUCKET_CONSUMER_TOKEN" ]]; then
      composer config --global bitbucket-oauth.bitbucket.org $BITBUCKET_CONSUMER_KEY $BITBUCKET_CONSUMER_TOKEN
    fi
    if [[ "$APPLICATION_ENV" == "development" ]] || [[ "$APP_ENV" == "local" ]]; then
      composer install -n --no-progress
    else
      composer install -n --no-progress --no-dev
    fi
  fi
fi

# Laravel Artisan Support
if [[ -z "$SKIP_LARAVEL_ARTISAN" ]]; then
  if [[ -f "$APP_ROOT/artisan" ]]; then
    # Make sure the app not running
    php artisan down
    # Do artisan key:generate ?
    if [[ "$ARTISAN_KEY_GENERATE" == "1" ]]; then
      php artisan key:generate
    fi
    # Do artisan vendor:publish ?
    if [[ "$ARTISAN_VENDOR_PUBLISH" == "1" ]]; then
      php artisan vendor:publish
    fi
    # Do artisan clear cache ?
    if [[ "$ARTISAN_CACHE_CLEAR" == "1" ]]; then
      php artisan config:clear
      php artisan route:clear
      php artisan view:clear
      php artisan event:clear
    fi
    # Do artisan cache ?
    if [[ "$ARTISAN_CACHE" == "1" ]]; then
      php artisan config:cache
      php artisan route:cache
      php artisan view:cache
      php artisan event:cache
    fi
    # Do artisan migrate ?
    if [[ "$ARTISAN_MIGRATE_FRESH" == "1" ]]; then
      php artisan migrate:fresh
    elif [[ "$ARTISAN_MIGRATE_FRESH_SEED" == "1" ]]; then
      php artisan migrate:fresh --seed
    elif [[ "$ARTISAN_MIGRATE_FORCE" == "1" ]]; then
      php artisan migrate --force
    elif [[ "$ARTISAN_MIGRATE" == "1" ]]; then
      php artisan migrate
    fi
    # Do artisan passport:install ?
    if [[ "$ARTISAN_PASSPORT_INSTALL" == "1" ]]; then
      php artisan passport:install
    fi
    # Set the app as running again
    php artisan up
    # Restart any queue worker
    php artisan queue:restart
  fi
fi

# Configure NPM
npm config set loglevel warn
npm config set progress false

# Laravel Mix NPM Support
if [[ -z "$SKIP_NPM" ]]; then
  # Try auto install for npm
  if [[ -f "$APP_ROOT/package-lock.json" ]] && [[ -f "/usr/bin/npm" ]]; then
    npm install --quiet
    if [[ "$NPM_RUN" == "1" ]]; then
      if [[ "$APPLICATION_ENV" == "development" ]] || [[ "$APP_ENV" == "local" ]]; then
        npm run --quiet --if-present dev
      else
        npm run --quiet --if-present prod
      fi
    fi
  fi
fi

# Run custom scripts
if [[ "$RUN_SCRIPTS" == "1" ]] ; then
  if [[ -d "$APP_ROOT/scripts/" ]]; then
    # make scripts executable incase they aren't
    chmod -Rf 750 $APP_ROOT/scripts/*; sync;
    # run scripts in number order
    for i in `ls $APP_ROOT/scripts/`; do $APP_ROOT/scripts/$i ; done
  else
    echo "Can't find script directory"
  fi
fi
