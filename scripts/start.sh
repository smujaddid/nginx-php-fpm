#!/bin/bash

# Force reassign APP_ROOT to /var/ww/html
APP_ROOT=/var/www/html

chown_app_root() {
  if [[ -z "$SKIP_CHOWN" ]]; then
    chown -Rf ${PHP_USER}.${PHP_GROUP} ${APP_ROOT}
  fi
}

# Setup custom user ID and group ID for php user
if [[ ! -z "$PUID" ]]; then
  if [[ -z "$PGID" ]]; then
    PGID=${PUID}
  fi
  deluser ${PHP_USER}
  addgroup -g ${PGID} ${PHP_GROUP}
  adduser -D -H -h ${PHP_HOME} -s /sbin/nologin -u ${PUID} -G ${PHP_GROUP} -g ${PHP_USER} ${PHP_USER}
  addgroup ${NGINX_USER} ${PHP_GROUP}
  chown_app_root
fi

# Set custom webroot
if [[ ! -z "$WEBROOT" ]]; then
  sed -i "s#root /var/www/html;#root ${WEBROOT};#g" /etc/nginx/sites-available/default.conf
  sed -i "s#root /var/www/html;#root ${WEBROOT};#g" /etc/nginx/sites-available/default-ssl.conf
else
  WEBROOT=$APP_ROOT
fi

# Enables 404 pages through php index
if [[ ! -z "$PHP_CATCHALL" ]]; then
  sed -i 's#try_files $uri $uri/ =404;#try_files $uri $uri/ /index.php?$args;#g' /etc/nginx/sites-available/default.conf
  sed -i 's#try_files $uri $uri/ =404;#try_files $uri $uri/ /index.php?$args;#g' /etc/nginx/sites-available/default-ssl.conf
fi

# Prevent config files from being filled to infinity by force of stop and restart the container
lastlinephpconf="$(grep "." /usr/local/etc/php-fpm.conf | tail -1)"
if [[ $lastlinephpconf == *"php_flag[display_errors]"* ]]; then
  sed -i '$ d' /usr/local/etc/php-fpm.conf
fi

# Display PHP error's or not
if [[ "$ERRORS" != "1" ]] ; then
  echo php_flag[display_errors] = off >> /usr/local/etc/php-fpm.d/www.conf
else
  echo php_flag[display_errors] = on >> /usr/local/etc/php-fpm.d/www.conf
fi

# Display Version Details or not
if [[ "$SHOW_NGINX_HEADERS" == "1" ]] ; then
  sed -i "s/server_tokens off;/server_tokens on;/g" /etc/nginx/nginx.conf
fi

# Pass real-ip to logs when behind ELB, etc
if [[ "$REAL_IP_HEADER" == "1" ]] ; then
  sed -i "s/#real_ip_header X-Forwarded-For;/real_ip_header X-Forwarded-For;/" /etc/nginx/sites-available/default.conf
  sed -i "s/#set_real_ip_from/set_real_ip_from/" /etc/nginx/sites-available/default.conf
  if [ ! -z "$REAL_IP_FROM" ]; then
    sed -i "s#172.16.0.0/12#${REAL_IP_FROM}#" /etc/nginx/sites-available/default.conf
  fi
fi
# Do the same for SSL sites
if [[ -f /etc/nginx/sites-available/default-ssl.conf ]]; then
  if [[ "$REAL_IP_HEADER" == "1" ]] ; then
    sed -i "s/#real_ip_header X-Forwarded-For;/real_ip_header X-Forwarded-For;/" /etc/nginx/sites-available/default-ssl.conf
    sed -i "s/#set_real_ip_from/set_real_ip_from/" /etc/nginx/sites-available/default-ssl.conf
    if [ ! -z "$REAL_IP_FROM" ]; then
      sed -i "s#172.16.0.0/12#${REAL_IP_FROM}#" /etc/nginx/sites-available/default-ssl.conf
    fi
  fi
fi

# Set the desired timezone
if [[ -f "/etc/TZ" ]]; then
  echo date.timezone=$(cat /etc/TZ) > /usr/local/etc/php/conf.d/timezone.ini
fi

# Display errors in docker logs
if [[ ! -z "$PHP_ERRORS_STDERR" ]]; then
  echo "log_errors = On" >> /usr/local/etc/php/conf.d/docker-vars.ini
  echo "error_log = /dev/stderr" >> /usr/local/etc/php/conf.d/docker-vars.ini
fi

# Increase the memory_limit
if [[ ! -z "$PHP_MEM_LIMIT" ]]; then
  sed -i "s/memory_limit = 128M/memory_limit = ${PHP_MEM_LIMIT}M/g" /usr/local/etc/php/conf.d/docker-vars.ini
fi

# Increase the post_max_size
if [[ ! -z "$PHP_POST_MAX_SIZE" ]]; then
  sed -i "s/post_max_size = 100M/post_max_size = ${PHP_POST_MAX_SIZE}M/g" /usr/local/etc/php/conf.d/docker-vars.ini
fi

# Increase the upload_max_filesize
if [[ ! -z "$PHP_UPLOAD_MAX_FILESIZE" ]]; then
  sed -i "s/upload_max_filesize = 100M/upload_max_filesize= ${PHP_UPLOAD_MAX_FILESIZE}M/g" /usr/local/etc/php/conf.d/docker-vars.ini
fi

# Increase the max_execution_time
if [[ ! -z "$PHP_MAX_EXECUTION_TIME" ]]; then
  sed -i "s/max_execution_time=600/max_execution_time= ${PHP_MAX_EXECUTION_TIME}/g" /usr/local/etc/php/conf.d/docker-vars.ini
fi

# Set php.ini
if [[ "$APPLICATION_ENV" == "development" ]] || [[ "$APP_ENV" == "local" ]]; then
  cp "${PHP_INI_DIR}/php.ini-development" "${PHP_INI_DIR}/php.ini"
else
  cp "${PHP_INI_DIR}/php.ini-production" "${PHP_INI_DIR}/php.ini"
fi

# Allow show php headers if flag set to "1"
if [[ "$SHOW_PHP_HEADERS" != "1" ]] ; then
  sed -i "s/expose_php = On/expose_php = Off/g" "${PHP_INI_DIR}/php.ini"
fi

# Make sure owned by php user before executing setup.sh
chown_app_root

# Run the setup script as php user
su-exec ${PHP_USER} '/setup.sh'

# Enable custom nginx config files if they exist
if [[ -f $APP_ROOT/conf/nginx/nginx.conf ]]; then
  cp ${APP_ROOT}/conf/nginx/nginx.conf /etc/nginx/nginx.conf
fi

if [[ -f $APP_ROOT/conf/nginx/nginx-site.conf ]]; then
  cp ${APP_ROOT}/conf/nginx/nginx-site.conf /etc/nginx/sites-available/default.conf
fi

if [[ -f $APP_ROOT/conf/nginx/nginx-site-ssl.conf ]]; then
  cp ${APP_ROOT}/conf/nginx/nginx-site-ssl.conf /etc/nginx/sites-available/default-ssl.conf
fi

# Supervisor
# if [[ -z "$SKIP_CONF_SUPERVISOR" ]]; then
#   if [[ -d "$APP_ROOT/conf/supervisor/" ]]; then
#     mkdir -p /etc/supervisor/conf.d/
#     cp $APP_ROOT/conf/supervisor/* /etc/supervisor/conf.d/
#     chown -Rf root.root /etc/supervisor/conf.d/*
#   fi
# fi

# Cron jobs
# if [[ -z "$SKIP_CONF_CRON" ]]; then
#   if [[ -f "$APP_ROOT/conf/crontab" ]]; then
#     cp $APP_ROOT/conf/crontab /etc/crontabs/${PHP_USER}
#     chown -Rf root.root /etc/crontabs/${PHP_USER}
#   fi
# fi

# Enable auto syncing git repo using cron job
if [[ "$AUTO_SYNC_GIT_REPO" == "1" ]]; then
  cp ${HOME}/cron/15min/auto-sync-git /etc/periodic/15min/auto-sync-git
  chown -Rf root.root /etc/periodic/15min/auto-sync-git
  chmod a+x /etc/periodic/15min/auto-sync-git
fi

# Run laravel worker with supervisor
if [[ "$RUN_LARAVEL_WORKER" == "1" ]]; then
  LARAVEL_WORKER_CONF_FILE=/etc/supervisor/conf.d/laravel-worker.conf
  if [[ "$APPLICATION_ENV" == "development" ]] || [[ "$APP_ENV" == "local" ]]; then
    cp /etc/supervisor/conf.d/laravel-worker.conf-development ${LARAVEL_WORKER_CONF_FILE}
  else
    cp /etc/supervisor/conf.d/laravel-worker.conf-production ${LARAVEL_WORKER_CONF_FILE}
  fi
  # Set the artisan path
  if [[ ! -z "$LARAVEL_ARTISAN_PATH" ]]; then
    sed -i "s#/var/www/html/artisan#${APP_ROOT}/${LARAVEL_ARTISAN_PATH}#g" ${LARAVEL_WORKER_CONF_FILE}
  fi
  # Set the worker command arguments
  if [[ ! -z "$LARAVEL_WORKER_ARGS" ]]; then
    sed -i "s/--sleep=3 --tries=3/${LARAVEL_WORKER_ARGS}/g" ${LARAVEL_WORKER_CONF_FILE}
  fi
  # Set user and group
  sed -i "s/user=php/user=${PHP_USER}/g" ${LARAVEL_WORKER_CONF_FILE}
  sed -i "s/group=php/group=${PHP_USER}/g" ${LARAVEL_WORKER_CONF_FILE}
  # Set number of processes
  if [[ ! -z "$LARAVEL_WORKER_NUMPROCS" ]]; then
    sed -i "s/numprocs=1/numprocs=${LARAVEL_WORKER_NUMPROCS}/g" ${LARAVEL_WORKER_CONF_FILE}
    sed -i "s/;process_name=%(program_name)s_%(process_num)02d/process_name=%(program_name)s_%(process_num)02d/g" ${LARAVEL_WORKER_CONF_FILE}
  fi
fi

# Run laravel websockets with supervisor
if [[ "$RUN_LARAVEL_WEBSOCKETS" == "1" ]]; then
  LARAVEL_WEBSOCKET_CONF_FILE=/etc/supervisor/conf.d/laravel-websockets.conf
  cp /etc/supervisor/conf.d/laravel-websockets.conf-default ${LARAVEL_WEBSOCKET_CONF_FILE}
  # Set the artisan path
  if [[ ! -z "$LARAVEL_ARTISAN_PATH" ]]; then
    sed -i "s#/var/www/html/artisan#${APP_ROOT}/${LARAVEL_ARTISAN_PATH}#g" ${LARAVEL_WEBSOCKET_CONF_FILE}
  fi
  # Set user and group
  sed -i "s/user=php/user=${PHP_USER}/g" ${LARAVEL_WEBSOCKET_CONF_FILE}
  sed -i "s/group=php/group=${PHP_USER}/g" ${LARAVEL_WEBSOCKET_CONF_FILE}
fi

# Run laravel scheduler with cron
if [[ "$RUN_LARAVEL_SCHEDULER" == "1" ]]; then
  PHP_USER_CRONTAB_FILE=/etc/crontabs/${PHP_USER}
  cp ${HOME}/crontab-php ${PHP_USER_CRONTAB_FILE}
  if [[ ! -z "$LARAVEL_SCHEDULER_SCHEDULE" ]]; then
    sed -i "s#*/5 * * * *#${LARAVEL_SCHEDULER_SCHEDULE}#g" ${PHP_USER_CRONTAB_FILE}
  fi
  # Set the artisan path
  if [[ ! -z "$LARAVEL_ARTISAN_PATH" ]]; then
    sed -i "s#/var/www/html/artisan#${APP_ROOT}/${LARAVEL_ARTISAN_PATH}#g" ${PHP_USER_CRONTAB_FILE}
  fi
fi

# Make sure owned by php user again
chown_app_root

# Start supervisord and services
exec /usr/bin/supervisord -n -c /etc/supervisord.conf
